# .rpb-openembedded-install-dependencies-tux: &rpb-openembedded-install-dependencies-tux |
set -ex
pkg_list="img2simg git pigz virtualenv wget"
DEBIAN_FRONTEND=noninteractive apt-get -q=2 update
DEBIAN_FRONTEND=noninteractive apt-get -q=2 install -y ${pkg_list}

#.rpb-openembedded-builders-tux: &rpb-openembedded-builders-tux |
# Get tuxsuite
virtualenv --python=$(which python3) .venv
source .venv/bin/activate
tuxsuite --version


[ "${DISTRO}" = "rpb" ] && IMAGES+=" ${IMAGES_RPB}"
[ "${DISTRO}" = "rpb-wayland" ] && IMAGES+=" ${IMAGES_RPB_WAYLAND}"

# These machines only build the basic rpb-console-image
case "${MACHINE}" in
  am57xx-evm|intel-core2-32|intel-corei7-64)
     IMAGES="rpb-console-image"
     ;;
esac

# Default value for BB_OVERRIDE is set to new syntax
if [ -z "${BB_OVERRIDE}" ]; then
    BB_OVERRIDE=':'
fi

# Create Tux Job file
cat << EOF > tux.json
{
  "sources": {
    "repo": {
        "url": "${MANIFEST_URL}",
        "branch": "${MANIFEST_BRANCH}",
        "manifest": "default.xml"
      }
  },
  "container": "ubuntu-20.04",
  "envsetup": "setup-environment",
  "distro": "${DISTRO}",
  "machine": "${MACHINE}",
  "target": "${IMAGES}",
  "artifacts": [],
    "environment": {
  },
  "local_conf": [
    "INHERIT += 'buildstats buildstats-summary'",
    "INHERIT${BB_OVERRIDE}remove = 'rm_work'",
    "IMAGE_NAME${BB_OVERRIDE}append = '-${CI_JOB_ID}'",
    "KERNEL_IMAGE_NAME${BB_OVERRIDE}append = '-${CI_JOB_ID}'",
    "MODULE_TARBALL_NAME${BB_OVERRIDE}append = '-${CI_JOB_ID}'",
    "DT_IMAGE_BASE_NAME${BB_OVERRIDE}append = '-${CI_JOB_ID}'",
    "BOOT_IMAGE_BASE_NAME${BB_OVERRIDE}append = '-${CI_JOB_ID}'"
  ]
}
EOF

# Process top level job extra local.conf
# This jq script will take the main JSON file and merge local_conf data
if [ -f "${CI_PROJECT_DIR}/local.conf.json" ]; then
    cp tux.json tux.orig.json
    jq 'reduce inputs.local_conf as $s (.; .local_conf += $s)' tux.orig.json "${CI_PROJECT_DIR}/local.conf.json" > tux.json
fi

# Build, do not report tuxsuite return code, since we want to process json out
tuxsuite bake submit --json-out status.json tux.json || true

# cleanup virtualenv
deactivate

if [ ! -f status.json ]; then
    echo "tux suite failed, infrastructure error, status.json does not exist"
    exit 1
fi

url=$(cat status.json | jq -r ".download_url")
state=$(cat status.json | jq -r ".state")
result=$(cat status.json | jq -r ".result")

if [ "$state" != "finished" ]; then
    echo "tuxsuite failed, with an unexpected reason"
    exit 1
fi

for log in build.log build-debug.log build-definition.json; do
  wget --no-verbose -O $CI_PROJECT_DIR/$log $url/$log
done


echo "TUXBUILD_URL=$url" > $CI_PROJECT_DIR/tuxbuild-url.txt
echo "TUXBUILD_URL=$url" > $CI_PROJECT_DIR/parameters

if [ "$result" != "pass" ]; then
    echo "tuxsuite build failed"
    exit 2
fi

DEPLOY_DIR_IMAGE=${PWD}/deploy_dir_images
echo "DEPLOY_DIR_IMAGE=${DEPLOY_DIR_IMAGE}" >> $CI_PROJECT_DIR/parameters

# files to download
wget --no-verbose $url/?export=json -O root.json
wget --no-verbose $url/images/${MACHINE}/?export=json -O images.json


mkdir ${DEPLOY_DIR_IMAGE} && pushd ${DEPLOY_DIR_IMAGE}
for j in ${CI_PROJECT_DIR}/root.json ${CI_PROJECT_DIR}/images.json; do
    for f in $(cat $j | jq -r .files[].Url); do
	wget --no-verbose $f
    done
done
popd

# FIXME: Sparse images here, until it gets done by OE
simg_cmd="img2simg"
case "${MACHINE}" in
  juno|stih410-b2260|orangepi-i96)
    ;;
  *)
    for rootfs in $(find ${DEPLOY_DIR_IMAGE} -type f -name *.rootfs.ext4.gz); do
      pigz -d -k ${rootfs}
      $simg_cmd ${rootfs%.gz} ${rootfs%.ext4.gz}.img
      rm -f ${rootfs%.gz}
      pigz -9 ${rootfs%.ext4.gz}.img
    done
    ;;
esac

# Create MD5SUMS file
pushd ${CI_PROJECT_DIR}
find ${DEPLOY_DIR_IMAGE} -type f | xargs md5sum > MD5SUMS.txt
sed -i "s|${DEPLOY_DIR_IMAGE}/||" MD5SUMS.txt
cp MD5SUMS.txt ${DEPLOY_DIR_IMAGE}
popd

# Need different files for each machine
BOOT_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "boot-*-${MACHINE}-*-${CI_JOB_ID}*.img" | sort | head -n 1 | xargs -r basename)
ROOTFS_EXT4_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "rpb-console-image-test-${MACHINE}-*-${CI_JOB_ID}.rootfs.ext4.gz" | xargs -r basename)
ROOTFS_TARXZ_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "rpb-console-image-test-${MACHINE}-*-${CI_JOB_ID}.rootfs.tar.xz" | xargs -r basename)
ROOTFS_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "rpb-console-image-test-${MACHINE}-*-${CI_JOB_ID}.rootfs.img.gz" | xargs -r basename)
ROOTFS_DESKTOP_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "rpb-desktop-image-test-${MACHINE}-*-${CI_JOB_ID}.rootfs.img.gz" | xargs -r basename)
KERNEL_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "*Image-*-${MACHINE}-*-${CI_JOB_ID}.bin" | xargs -r basename)
DISK_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "${IMAGES}-${MACHINE}-*-${CI_JOB_ID}.rootfs.wic.gz" | xargs -r basename)

case "${MACHINE}" in
  am57xx-evm)
    # LAVA image is too big for am57xx-evm
    ROOTFS_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "rpb-console-image-${MACHINE}-*-${CI_JOB_ID}.rootfs.img.gz" | xargs -r basename)
    # FIXME: several dtb files case
    ;;
  intel-core2-32|intel-corei7-64)
    # No LAVA testing on intel-core* machines
    ROOTFS_TARXZ_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "rpb-console-image-${MACHINE}-*-${CI_JOB_ID}.rootfs.tar.xz" | xargs -r basename)
    ;;
  juno)
    # FIXME: several dtb files case
    ;;
  *)
    DTB_IMG=$(find ${DEPLOY_DIR_IMAGE} -type f -name "*-${MACHINE}-*-${CI_JOB_ID}.dtb" | xargs -r basename)
    ;;
esac

# Note: the main job script allows to override the default value for
#       PUBLISH_SERVER and PUB_DEST, typically used for OE RPB builds
DEPLOY_DIR_IMAGE=${DEPLOY_DIR_IMAGE}
MANIFEST_COMMIT=${MANIFEST_COMMIT}
BOOT_URL=${PUBLISH_SERVER}/${PUB_DEST}/${BOOT_IMG}
ROOTFS_BUILD_URL=${PUBLISH_SERVER}/${PUB_DEST}/${ROOTFS_EXT4_IMG}
ROOTFS_SPARSE_BUILD_URL=${PUBLISH_SERVER}/${PUB_DEST}/${ROOTFS_IMG}
ROOTFS_DESKTOP_SPARSE_BUILD_URL=${PUBLISH_SERVER}/${PUB_DEST}/${ROOTFS_DESKTOP_IMG}
SYSTEM_URL=${PUBLISH_SERVER}/${PUB_DEST}/${ROOTFS_EXT4_IMG}
KERNEL_URL=${PUBLISH_SERVER}/${PUB_DEST}/${KERNEL_IMG}
DTB_URL=${PUBLISH_SERVER}/${PUB_DEST}/${DTB_IMG}
NFSROOTFS_URL=${PUBLISH_SERVER}/${PUB_DEST}/${ROOTFS_TARXZ_IMG}
RECOVERY_IMAGE_URL=${PUBLISH_SERVER}/${PUB_DEST}/juno-oe-uboot.zip
LXC_BOOT_IMG=${BOOT_IMG}
LXC_ROOTFS_IMG=$(basename ${ROOTFS_IMG} .gz)
INITRD_URL="${INITRD_URL}"
KERNEL_ARGS=""
KERNEL_REPO=$KERNEL_REPO
KERNEL_BRANCH=$KERNEL_BRANCH
KERNEL_COMMIT=$KERNEL_COMMIT

# .rpb-openembedded-publishers: &rpb-openembedded-publishers |
set -ex
# install jflog client tool, v1, used for publishing artifacts
(mkdir -p $HOME/bin && cd $HOME/bin && curl -fL https://getcli.jfrog.io | sh)

if [ -f ${CI_PROJECT_DIR}/BUILD-INFO.txt ];then
    BUILD_INFO="--build-info ${CI_PROJECT_DIR}/BUILD-INFO.txt"
else
    BUILD_INFO=""
fi

if [ -z "${DEPLOY_DIR_IMAGE}" ] || [ -z "${PUB_DEST}" ] || [ -z "${PUBLISH_SERVER}" ]
then
    echo "== missing publishing variables =="
    echo "DEPLOY_DIR_IMAGE = ${DEPLOY_DIR_IMAGE}"
    echo "PUB_DEST         = ${PUB_DEST}"
    echo "PUBLISH_SERVER   = ${PUBLISH_SERVER}"
    exit 1
fi

# Publish
echo "JFROG_ARTIFACTORY_URL=${PUBLISH_SERVER}/${PUB_DEST}" > $CI_PROJECT_DIR/jfrog-artifactory-url.txt
time ${HOME}/bin/jfrog rt u \
  --flat=true --include-dirs=true --symlinks=true --detailed-summary \
  --dry-run=${JGROG_DRY_RUN} \
  --apikey ${JFROG_TOKEN} \
  --url ${PUBLISH_SERVER}/ \
  ${DEPLOY_DIR_IMAGE}/ ${PUB_DEST}/

.rpb-openembedded-submit-for-testing: &rpb-openembedded-submit-for-testing |
# Only test in case artifacts were uploaded
test "${JGROG_DRY_RUN}" = "true" && exit

# Create variables file to use with lava-test-plans submit_for_testing.py
function create_testing_variables_file () {
	cat << EOF > $1
"LAVA_JOB_PRIORITY": "$LAVA_JOB_PRIORITY"

"PROJECT": "projects/lt-qcom/"
"PROJECT_NAME": "lt-qcom"
"OS_INFO": "$OS_INFO"

"BUILD_URL": "$CI_JOB_URL"
"BUILD_NUMBER": "$CI_JOB_ID"

"DEPLOY_OS": "$DEPLOY_OS"
"BOOT_URL": "$BOOT_URL"
"BOOT_URL_COMP": "$BOOT_URL_COMP"
"LXC_BOOT_FILE": "$LXC_BOOT_FILE"
"ROOTFS_URL": "$ROOTFS_URL"
"ROOTFS_URL_COMP": "$ROOTFS_URL_COMP"
"LXC_ROOTFS_FILE": "$LXC_ROOTFS_FILE"

"SMOKE_TESTS": "$SMOKE_TESTS"
"WIFI_SSID_NAME": "LAVATESTX"
"WIFI_SSID_PASSWORD": "NepjqGbq"
"WLAN_DEVICE": "$WLAN_DEVICE"
"WLAN_TIME_DELAY": "$WLAN_TIME_DELAY"
"ETH_DEVICE": "$ETH_DEVICE"
"PM_QA_TESTS": "$PM_QA_TESTS"
EOF
}

rm -rf lava-test-plans
if [ "$LAVA_TEST_PLANS_GIT_REPO" ]; then
  git clone --depth 1 $LAVA_TEST_PLANS_GIT_REPO lava-test-plans
else
  git clone --depth 1 https://github.com/Linaro/lava-test-plans.git
fi
export LAVA_TEST_CASES_PATH=$(realpath lava-test-plans)
pip3 install -r "$LAVA_TEST_CASES_PATH/requirements.txt"

# Record version used
(cd lava-test-plans && git rev-parse HEAD)

# main parameters
export DEPLOY_OS=oe

# remove qcom/ prefix on MANIFEST_BRANCH variable
export OS_INFO=openembedded-${DISTRO}-${MANIFEST_BRANCH#qcom/}
export QA_SERVER_PROJECT=openembedded-rpb-${MANIFEST_BRANCH#qcom/}

# boot and rootfs parameters, BOOT_URL comes from builders.sh
# and has not compression
export BOOT_URL_COMP=
export LXC_BOOT_FILE=$(basename ${BOOT_URL})

export LAVA_JOB_PRIORITY="medium"

case "${MACHINE}" in
  dragonboard-410c|dragonboard-820c|dragonboard-845c)
    export DEVICE_TYPE="${MACHINE}"

    # Tests settings, thermal fails in db410c
    if [ ${DEVICE_TYPE} = "dragonboard-410c" ]; then
      export PM_QA_TESTS="cpufreq cpuidle cpuhotplug cputopology"
      export WLAN_DEVICE="wlan0"
      export WLAN_TIME_DELAY="0s"
      export ETH_DEVICE="eth0"
    elif [ ${DEVICE_TYPE} = "dragonboard-820c" ]; then
      export PM_QA_TESTS="cpufreq cputopology"
      export WLAN_DEVICE="wlp1s0"
      export WLAN_TIME_DELAY="15s"
      export ETH_DEVICE="enP2p1s0"
    elif [ ${DEVICE_TYPE} = "dragonboard-845c" ]; then
      export PM_QA_TESTS="cpufreq cpuidle cpuhotplug cputopology"
      export WLAN_DEVICE="wlan0"
      export WLAN_TIME_DELAY="15s"
      export ETH_DEVICE="enp1s0u3"
    fi
    export SMOKE_TESTS="pwd, uname -a, ip a, vmstat, lsblk"

    export ROOTFS_URL=${ROOTFS_SPARSE_BUILD_URL}
    export ROOTFS_URL_COMP="gz"
    export LXC_ROOTFS_FILE=$(basename ${ROOTFS_URL} .gz)

    create_testing_variables_file ${CI_PROJECT_DIR}/submit_for_testing.yaml
    case "${DISTRO}" in
      rpb)
        cd lava-test-plans
        python3 -m lava_test_plans \
            --device-type ${DEVICE_TYPE} \
            --build-number ${CI_JOB_ID} \
            --lava-server ${LAVA_SERVER} \
            --qa-server ${QA_SERVER} \
            --qa-server-team ${QA_SERVER_GROUP} \
            --qa-server-project ${QA_SERVER_PROJECT} \
            --testplan-device-path lava_test_plans/projects/lt-qcom/devices \
            ${DRY_RUN} \
            --test-case testcases/distro-smoke.yaml testcases/bt.yaml testcases/wifi.yaml \
            --variables ${CI_PROJECT_DIR}/submit_for_testing.yaml
      ;;
      rpb-wayland)
        echo "Currently no tests for rpb-wayland"
      ;;
    esac
    ;;
  *)
    echo "Skip DEVICE_TYPE for ${MACHINE}"
    ;;
esac
